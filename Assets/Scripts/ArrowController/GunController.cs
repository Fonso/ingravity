using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ArrowController))]
public class GunController : Gun
{
    public  CustomGravity ObjectSelected { get { return _selected; } }


    private ArrowController _arrowController;
    
    private void Start()
    {
        _arrowController = GetComponent<ArrowController>(); 
    }

    protected override void OnPreHit(CustomGravity gravity_comp)
    {
        base.OnPreHit(gravity_comp);
        if (_selected != null)
        {
            _selected.Unfreeze();
        }

        if(_selected != gravity_comp) 
        {
            Hit(); 
        }

    }

    protected override void OnPostHit()
    {
        base.OnPostHit();
        
        if (_arrowController.ArrowSelected != null)
        {
            _arrowController.DisableArrows();
            _selected.ChangeGravityDirection(_arrowController.ArrowSelected.ArrowDirection);
            _selected = null;
        }
    }

    private void Hit()
    {
        _arrowController.DisableArrows();
        _selected.Freeze();
        _arrowController.RepositionArrows(_selected.transform.position, _selected.transform.localScale); 
    }
}
