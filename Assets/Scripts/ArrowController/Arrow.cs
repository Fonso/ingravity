using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{
    public CustomGravity.GravityDirections ArrowDirection { get { return _arrowDirection;  } set { _arrowDirection = value; } }
    
    private CustomGravity.GravityDirections _arrowDirection;

    private Material _mat; 



    // Start is called before the first frame update
    void Start()
    {
        _mat = GetComponentInChildren<Renderer>().material;
    }

    public void ChangeMaterialAlpha(float alpha_percent) 
    {
        if (_mat.color.a != alpha_percent)
        {
            Color color = _mat.color;
            color.a = alpha_percent;
            _mat.color = color;
        }
    }

    public void ChangeMaterial(Color material_color ,float alpha_percent)
    {
        if (_mat.color.a != alpha_percent)
        {
            Color color = material_color;
            color.a = alpha_percent;
            _mat.color = color;
        }
    }

}
