using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{
    public Arrow ArrowPrefab;
    public Arrow ArrowSelected { get { return _lastArrowSelected; } }
    private Arrow _lastArrowSelected = null;
    private List<Arrow> _arrowList = new List<Arrow>();

    public  Color HighlightColor;
    private Color _oldHighLightColor; 


    private void Start()
    {
        SpawnArrows();  
    }

    private void Update()
    {
        HighLightArrow(); 
    }

    private void HighLightArrow()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity))
        {
            if (hit.collider.CompareTag("Arrow"))
            {
                if (!_lastArrowSelected)
                {
                    Debug.Log(hit);
                    _lastArrowSelected = hit.collider.gameObject.GetComponent<Arrow>();
                    _lastArrowSelected.ChangeMaterial(HighlightColor, 0.75f);
                }
                return;
            }

            else
            {
                if (_lastArrowSelected)
                {
                    _lastArrowSelected.ChangeMaterial( _oldHighLightColor, 0.10f);
                    _lastArrowSelected = null;
                }
            }
        }
    }

    private void SpawnArrows()
    {
        CreateArrow(CustomGravity.GravityDirections.down);
        CreateArrow(CustomGravity.GravityDirections.up);
        CreateArrow(CustomGravity.GravityDirections.right);
        CreateArrow(CustomGravity.GravityDirections.left);
        CreateArrow(CustomGravity.GravityDirections.forward);
        CreateArrow(CustomGravity.GravityDirections.back);
    }

    public void RepositionArrows(Vector3 center, Vector3 localScale) 
    {
        foreach(Arrow ar in _arrowList) 
        {
            Vector3 dir = CustomGravity.GravityDirectionToVector3(ar.ArrowDirection);

            float scale = new Vector3(dir.x * localScale.x, dir.y * localScale.y, dir.z * localScale.z).magnitude;
            ar.gameObject.transform.position = center + (dir * scale / 2.0f);
            ar.gameObject.SetActive(true);
        } 
    }

    public void DisableArrows()
    {
        foreach (Arrow a in _arrowList)
        {
            a.gameObject.SetActive(false); 
        }
    }

    public GameObject CreateArrow(CustomGravity.GravityDirections direction)
    {
        Vector3 dir = CustomGravity.GravityDirectionToVector3(direction);

        Vector3 position = transform.position; 

        GameObject g = Instantiate(ArrowPrefab.gameObject, position, Quaternion.LookRotation(dir));
        g.SetActive(false);

        Arrow arrowComponent = g.GetComponent<Arrow>();
        arrowComponent.ArrowDirection = direction;

        _arrowList.Add(arrowComponent);

        return g;
    }

}
