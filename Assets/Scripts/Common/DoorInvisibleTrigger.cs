using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInvisibleTrigger : MonoBehaviour
{
    public Door Door;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) 
        {
            if(Door)Door.Open(false); 
        }
    }

}
