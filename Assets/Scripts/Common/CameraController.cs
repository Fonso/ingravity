﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    /// <summary>
    /// Controla si queremos bloquear o no el movimiento del cursor del jugador en el centro de la pantalla
    /// </summary>
    private CursorLockMode _lockMode;
    [Header("Movimiento del jugador")]
    /// <summary>
    /// Sensibilidad del movimiento de la camara en X
    /// </summary>
    [Tooltip("Sensibilidad del movimiento de la camara en X")]
    public float SensitivityX;
    /// <summary>
    /// Sensibilidad del movimiento de la camara en Y 
    /// </summary>
    [Tooltip("Sensibilidad del movimiento de la camara en Y")]
    public float SensitivityY;

    private bool _lockRotation; 

    /// <summary>
    /// Angulo maximo que puedes mover la camara en el eje Y. Sirve para solucionar el problema del gimbal lock e impedir que el 
    /// jugador haga giros de camara sin sentido
    /// </summary>
    [Header("Angulo maximo que puedes mover la camara en el eje Y")]
    [Tooltip("Sensibilidad del movimiento de la camara en Y")]
    [Range(0, 160)]
    public float  MaxAngle;
    /// <summary>
    /// Variable para guardar el valor del angulo minimo que puede llegar a tener la cámara en Y
    /// </summary>
    private float minCameraAngle;
    /// <summary>
    /// Variable para guardar el valor del angulo máximo que puede llegar a tener la cámara en Y
    /// </summary>
    private float maxCameraAngle;

    private void Start()
    { 
        _lockMode = CursorLockMode.Locked;
        CalculateCameraAngles(); 
    }

    // Update is called once per frame
    void Update()
    {
        //To prevent the player to move the cursor out
        Cursor.lockState = _lockMode;
        RotateCamera(); 
    }

    /// <summary>
    /// Calcula el angulo máximo y minimo que puede tener la cámara en el eje Y
    /// </summary>
    private void CalculateCameraAngles() 
    {
        float halfAngle = MaxAngle / 2.0f;
        minCameraAngle  = halfAngle;
        maxCameraAngle =  360.0f - halfAngle; 
    }

    /// <summary>
    /// Rota la cámara en función del movimiento del ratón del jugador y su sensibilidad en X e Y. 
    /// </summary>
    private void RotateCamera() 
    {
        if (!_lockRotation)
        {

            float mouseX = Input.GetAxis("Mouse X") * SensitivityX;
            float mouseY = Input.GetAxis("Mouse Y") * SensitivityY;

            transform.Rotate(-mouseY, mouseX, 0);

            float adjustedEulerX = transform.eulerAngles.x;

            if (adjustedEulerX <= 81.0f)
            {
                adjustedEulerX = Mathf.Clamp(adjustedEulerX, 0.0f, minCameraAngle);
            }
            else if (adjustedEulerX >= 279.0f)
            {
                adjustedEulerX = Mathf.Clamp(adjustedEulerX, maxCameraAngle, 360.0f);
            }

            transform.rotation = Quaternion.Euler(adjustedEulerX,
                                                              transform.eulerAngles.y,
                                                              0.0f);
        }
    }

    public void LockCamera(bool lock_cam)
    {
        _lockRotation = lock_cam; 
    }

    /// <summary>
    /// Cambia el modo del bloqueo del cursor. 
    /// </summary>
    /// <param name="lockMode"> Tipo de bloqueo </param>
    public void ChangeCursorLockMode(CursorLockMode lockMode)
    {
        _lockMode = lockMode; 
    }

}
