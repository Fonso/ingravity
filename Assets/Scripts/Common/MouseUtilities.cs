using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems; 

public class MouseUtilities : PointerInputModule
{
    public GameObject currentFocusedGO;

    public override void Process()
    {
        ProcessMouseEvent();
    }

    private void ProcessMouseEvent() 
    {
        var mouseData = GetMousePointerEventData(0);
        var leftButtonData = mouseData.GetButtonState(PointerEventData.InputButton.Left).eventData;

        currentFocusedGO = leftButtonData.buttonData.pointerCurrentRaycast.gameObject;
    }

}
