using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonTrigger : MonoBehaviour
{
    public enum Color { Blue, Red}
    public Color ButtonColor; 
    public bool IsActive = false;
    public UnityEvent onActivate;
    public UnityEvent onDeactivate;

    Animator _anim; 


    private void Start()
    {
        _anim = transform.parent.GetComponentInChildren<Animator>();  
        this.transform.tag = ColorToTag(ButtonColor); 
    }

    private string ColorToTag(Color btc) 
    {
        switch (btc) 
        {
            case Color.Blue:
                return "Blue";
            case Color.Red:
                return "Red"; 
        }
        return "None"; 
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag(this.tag)) 
        {
            Debug.Log("This " + this.tag + " button is activated"); 
            _anim.SetTrigger("Active");
            IsActive = true;
            onActivate.Invoke(); 
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(this.tag))
        {
            Debug.Log("This " + this.tag + " button is unactivated");
            _anim.SetTrigger("UnActive");
            IsActive = false;
            onDeactivate.Invoke(); 
        }
    }


}
