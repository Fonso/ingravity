using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomGravity : MonoBehaviour
{
    public enum GravityDirections { down, up, right, left, forward, back, zero}

    public GravityDirections GravityDirection = GravityDirections.down; 
    private GravityDirections _originalGravityDirection; 
    private Vector2 _lastGravityDirection = Vector3.zero; 
    private Vector3 _gravityDirection = Vector3.down;
    private Vector3 _originalGravity;
    private Vector3 _originalPosition;
    private Quaternion _originalRotation;
    private static float _maxDegrees = 70.0f;
    private static float _halfMaxDegrees; 

    private Rigidbody _rb; 
    public static float GravityForce = 9.8f; 

    // Start is called before the first frame update
    void Start()
    {
        _originalGravityDirection = GravityDirection; 
        _originalGravity = ToAbsoluteVec3(GravityDirection);
        _originalPosition = transform.position;
        _originalRotation = transform.rotation;
        _halfMaxDegrees = _maxDegrees; 
        _rb = GetComponent<Rigidbody>();
        _rb.useGravity = false; 
        ChangeGravityDirection(GravityDirection);
        _originalGravity = _gravityDirection; 
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ApplyGravity(); 
    }


    private void ApplyGravity() 
    {
        //Gravity = M * A 
        _rb.AddForce(_rb.mass * (_gravityDirection.normalized * GravityForce * (Time.fixedDeltaTime * 100)), ForceMode.Force);
    }

    public void Unfreeze() 
    {
        _gravityDirection = _lastGravityDirection; 
    }

    public void Freeze() 
    {
        _lastGravityDirection = _gravityDirection; 
        _rb.velocity = _gravityDirection = Vector3.zero;
    }

    public static Vector3 GravityDirectionToVector3(GravityDirections new_direction) 
    {
        switch (new_direction)
        {
            case GravityDirections.up:
                return Vector3.up;

            case GravityDirections.forward:
                return Vector3.forward;        

            case GravityDirections.back:
                return -Vector3.forward;

            case GravityDirections.down:
                return Vector3.down;

            case GravityDirections.right:
                return Vector3.right;

            case GravityDirections.left:
                return Vector3.left;
            case GravityDirections.zero:
                return Vector3.zero; 
        }
        return Vector3.zero; 
    }


    public static Vector3 ToAbsoluteVec3(GravityDirections new_direction)
    {
        switch (new_direction)
        {
            case GravityDirections.up:
                return Vector3.up;

            case GravityDirections.forward:
                return Vector3.forward;

            case GravityDirections.back:
                return -Vector3.forward;

            case GravityDirections.down:
                return Vector3.down;

            case GravityDirections.right:
                return Vector3.right;

            case GravityDirections.left:
                return Vector3.left;
            case GravityDirections.zero:
                return Vector3.zero;
        }
        return Vector3.zero;
    }

    public void ChangeGravityDirection(GravityDirections new_direction) 
    {
        _gravityDirection = ToAbsoluteVec3(new_direction);
        GravityDirection = new_direction; 
    }


    public Vector3 GravityDirectionToRelativeVector3(GravityDirections new_direction, Vector3 position) 
    {
        if(new_direction == GravityDirections.up || new_direction == GravityDirections.down) 
        {
            return ToAbsoluteVec3(new_direction); 
        }

        else 
        {
            Vector3 direction = position - this.transform.position;
            direction.y = 0;
            direction.Normalize();


            float frontAngle = Mathf.Acos(Vector3.Dot(Vector3.back, direction)) * 100.0f;
            float backAngle  = Mathf.Acos(Vector3.Dot(Vector3.forward, direction)) * 100.0f;
            float rightAngle = Mathf.Acos(Vector3.Dot(Vector3.right, direction)) * 100.0f;
            float leftAngle  = Mathf.Acos(Vector3.Dot(Vector3.left, direction)) * 100.0f;

            //Debug.Log("Front: " + frontAngle);
            //Debug.Log("Back: " + backAngle);

            //Debug.Log("Right: " + rightAngle);
            //Debug.Log("Left: " + leftAngle);


            if (frontAngle <= _halfMaxDegrees) 
            {
                Debug.Log("Front Angel " + frontAngle);
                switch (new_direction) 
                {
                    case GravityDirections.forward:
                        return Vector3.forward; 
                        
                    case GravityDirections.right:
                        return Vector3.right;

                    case GravityDirections.back:
                        return Vector3.back;

                    case GravityDirections.left:
                        return Vector3.left;
                }
            }

            if (rightAngle <= _halfMaxDegrees)
            {
                Debug.Log("Right Angel " + rightAngle);
                switch (new_direction)
                {
                    case GravityDirections.forward:
                        return Vector3.left;

                    case GravityDirections.right:
                        return Vector3.forward;

                    case GravityDirections.back:
                        return Vector3.right;

                    case GravityDirections.left:
                        return Vector3.left;
                }
            }

            if (leftAngle <= _halfMaxDegrees)
            {
                Debug.Log("Left Angel " + leftAngle);
                switch (new_direction)
                {
                    case GravityDirections.forward:
                        return Vector3.right;

                    case GravityDirections.right:
                        return Vector3.forward;

                    case GravityDirections.back:
                        return Vector3.left;

                    case GravityDirections.left:
                        return Vector3.back;
                }
            }

            if (backAngle <= _halfMaxDegrees)
            {
                Debug.Log("Back Angel " + backAngle);
                switch (new_direction)
                {
                    case GravityDirections.forward:
                        return Vector3.back;

                    case GravityDirections.right:
                        return Vector3.left;

                    case GravityDirections.back:
                        return Vector3.forward;

                    case GravityDirections.left:
                        return Vector3.right;
                }
            }

            return Vector3.zero; 
        }
    }

    public static Vector3 PointToObject(GravityDirections new_direction, Vector3 playerPos, Vector3 objPosition)
    {
        if (new_direction == GravityDirections.up || new_direction == GravityDirections.down)
        {
            return ToAbsoluteVec3(new_direction);
        }

        Vector3 direction = objPosition - playerPos;
        direction.y = 0;
        direction.Normalize();
        return direction;
    }

    public static Vector3 ToRelativeVector3(GravityDirections new_direction, Vector3 playerPos, Vector3 objPosition)
    {
        if (new_direction == GravityDirections.up || new_direction == GravityDirections.down)
        {
            return ToAbsoluteVec3(new_direction);
        }

        else
        {
            Vector3 direction = playerPos - objPosition;
            direction.y = 0;
            direction.Normalize();


            float frontAngle = Mathf.Acos(Vector3.Dot(Vector3.back, direction)) * 100.0f;
            float backAngle = Mathf.Acos(Vector3.Dot(Vector3.forward, direction)) * 100.0f;
            float rightAngle = Mathf.Acos(Vector3.Dot(Vector3.right, direction)) * 100.0f;
            float leftAngle = Mathf.Acos(Vector3.Dot(Vector3.left, direction)) * 100.0f;

            if (frontAngle <= _halfMaxDegrees)
            {
#if NOT_IGNORE_LOGS
                Debug.Log("Front Angel " + frontAngle);
#endif
                return ToAbsoluteVec3(new_direction); 
            }

            if (rightAngle <= _halfMaxDegrees)
            {
#if NOT_IGNORE_LOGS
                Debug.Log("Right Angel " + rightAngle);
#endif
                switch (new_direction)
                {
                    case GravityDirections.forward:
                        return Vector3.right;

                    case GravityDirections.right:
                        return Vector3.back;

                    case GravityDirections.back:
                        return Vector3.left;

                    case GravityDirections.left:
                        return Vector3.forward;
                }
            }

            if (leftAngle <= _halfMaxDegrees)
            {
#if NOT_IGNORE_LOGS
                Debug.Log("Left Angel " + leftAngle);
#endif
                switch (new_direction)
                {
                    case GravityDirections.forward:
                        return Vector3.left;

                    case GravityDirections.right:
                        return Vector3.forward;

                    case GravityDirections.back:
                        return Vector3.right;

                    case GravityDirections.left:
                        return Vector3.back;
                }
            }

            if (backAngle <= _halfMaxDegrees)
            {
#if NOT_IGNORE_LOGS
                Debug.Log("Back Angel " + backAngle);
#endif
                switch (new_direction)
                {
                    case GravityDirections.forward:
                        return Vector3.back;

                    case GravityDirections.right:
                        return Vector3.left;

                    case GravityDirections.back:
                        return Vector3.forward;

                    case GravityDirections.left:
                        return Vector3.right;
                }
            }

            return Vector3.zero;
        }
    }

    public void ChangeGravityDirection(GravityDirections new_direction, GameObject player) 
    {
        _gravityDirection = GravityDirectionToRelativeVector3(new_direction, player.transform.position);
        GravityDirection = new_direction; 
    }

    public void ResetObject() 
    {
        transform.position = _originalPosition;
        transform.rotation = _originalRotation; 
        _gravityDirection  = _originalGravity;
        GravityDirection = _originalGravityDirection;   
    }

    public void ResetGravity() 
    {
        _gravityDirection = _originalGravity;
        GravityDirection = _originalGravityDirection;
    }

}
