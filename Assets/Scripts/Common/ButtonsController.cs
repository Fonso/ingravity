using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsController : MonoBehaviour
{
    public List<ButtonTrigger> buttons;
    public Door Door; 
    // Start is called before the first frame update
    void Start()
    {
        SubscribeOnActivate();    
    }

    private void SubscribeOnActivate() 
    {
        if (buttons.Count > 0)
        {
            foreach (ButtonTrigger b in buttons)
            {
                b.onActivate.AddListener(CheckOpenDoor);
            }
        }
    }

    private void CheckOpenDoor() 
    {
        foreach(ButtonTrigger b in buttons) 
        {
            if (!b.IsActive) 
            {
                return; 
            }    
        }
        if (Door)
        {
            Door.Open(true);
        }
        else { Debug.Log("Door is null"); }
    }
}
