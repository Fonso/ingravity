using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour
{
    public string DialogName; 
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player")) 
        {
            DialogController.Instance.BeginDialog(DialogName); 
            this.gameObject.SetActive(false); 
        }
    }
}
