using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platforms : MonoBehaviour
{
    private Rigidbody _playerRB;
    Rigidbody _rb;
    Vector3 lastPosition;
    Vector3 tripaloski; 

    private void Start()
    {
        _rb = GetComponent<Rigidbody>();  
    }

    public void FixedUpdate()
    {
        tripaloski = (lastPosition - _rb.position).normalized;
        if (_playerRB) 
        {
            Vector3 movDirection = lastPosition - _rb.position;

            Debug.Log(movDirection); 
            _playerRB.AddForce(movDirection.normalized, ForceMode.VelocityChange); 
            lastPosition = _rb.position; 
        }
        lastPosition = _rb.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) 
        {
            Debug.Log("Player is on the platform");
            _playerRB = other.transform.parent.GetComponent<Rigidbody>();
            lastPosition = _rb.position; 
        } 
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player")) 
        {
            Debug.Log("Player is NOT on the platform");
            _playerRB = null; 
        }
    }
}
