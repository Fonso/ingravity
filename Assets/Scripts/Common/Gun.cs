using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public enum Click{ Left, Right }

    public ParticleSystem GunSplash;
    protected RaycastHit _lastHit;
    protected CustomGravity _selected;
    public bool LockPrimaryAt = false;
    public Click PrimaryClick = Click.Left;
    private float PrimaryClickCD; 

    protected virtual void Update()
    {
        Shoot(); 
    }

    private void Shoot()
    {
        if (!LockPrimaryAt && Input.GetMouseButtonDown((int)PrimaryClick))
        {
            if (Physics.Raycast(transform.position, transform.forward, out _lastHit, Mathf.Infinity))
            {
                Instantiate(GunSplash, _lastHit.point, Quaternion.identity);
                CustomGravity gravity_component = _lastHit.collider.gameObject.GetComponent<CustomGravity>();

                if (gravity_component)
                {
                    OnPreHit(gravity_component); 

                    _selected = gravity_component;
                }
                OnPostHit(); 
            }
        }
    }

    public void UnlockPrimaryAttack() 
    {
        LockPrimaryAt = false; 
    }

    protected virtual void OnPostHit() 
    { 
    }

    protected virtual void OnPreHit(CustomGravity gravity_comp) 
    {
        
    }

}
