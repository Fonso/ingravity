using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    public Door _door;
    public bool PlayerNotTrigger = true; 
    private enum ButtonState { Pushed, UnPushed}
    private ButtonState State = ButtonState.UnPushed;
    private Animator _anim;

    private void Start()
    {
        _anim = this.transform.parent.GetComponentInChildren<Animator>();  
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Object"))
        {
            Debug.Log("Button pushed");
            _anim.SetTrigger("Active");
            if (_door) { _door.Open(true); }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Object"))
        {
            Debug.Log("Button unpushed");
            _anim.SetTrigger("UnActive");
            if (_door) { _door.Open(false); }
        }
    }

  
}
