using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = ("New Dialog"), menuName = ("Dialog"))]
public class Dialog : ScriptableObject
{
    public string Id;
    [TextArea(5, 30)]
    public string[] Dialogs;
}
