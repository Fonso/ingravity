using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro; 

public class DialogController : MonoBehaviour
{
    public List<Dialog> Dialogs;
    public TextMeshProUGUI DisplayText;
    public float DisplaySpeed;
    public float TimeBetweenDialogs;
    public float timeBetweenPoints; 

    private static DialogController _instance;
    private IEnumerator _actualDialogCorroutine; 
    private bool isDialogRunning = false;
    private bool _pointFound = false; 

    public static DialogController Instance 
    { 
        get 
        {
            return _instance;
        } 
    }

    private void Awake()
    {
        if (!_instance) 
        {
            _instance = this;
        }
    }

    public bool BeginDialog(string dialogName) 
    {
        if (dialogName == "Clear") { DisplayText.text = ""; return true;}
        if (_actualDialogCorroutine != null) { StopCoroutine(_actualDialogCorroutine); }
        _actualDialogCorroutine = BeginDialogCorroutine(dialogName);
        StartCoroutine(_actualDialogCorroutine); 
        return true; 
    } 

    private IEnumerator BeginDialogCorroutine(string dialogName) 
    {
        Dialog dialogToShow = Dialogs.Find(x => x.Id == dialogName);
        if (dialogToShow)
        {
            isDialogRunning = true;
            DisplayText.text = "";
            Debug.Log("Dialog Found!");
            bool writtingRichText = false;
            bool firstSignalFound = false; 
            List<char> richText = new List<char>();
            for (int i = 0; i < dialogToShow.Dialogs.Length; i++)
            {
                char[] splittedLetters = dialogToShow.Dialogs[i].ToCharArray();
                foreach (char letter in splittedLetters)
                {
                    if (writtingRichText || letter == '<')
                    { 
                            if (!writtingRichText) 
                            {
                                richText.Clear(); 
                                writtingRichText = true;
                            }
                     
                            richText.Add(letter);

                            if(letter == '>' && !firstSignalFound) { firstSignalFound = true;  }
                            else if(letter == '>' && firstSignalFound) 
                            {
                                DisplayText.text += new string(richText.ToArray());
                                writtingRichText = false;
                                firstSignalFound = false;
                            }
                        }

                        else
                        {
                            DisplayText.text += letter;
                            if (letter == '.' || _pointFound)
                            {
                                _pointFound = true;
                                yield return new WaitForSeconds(timeBetweenPoints);
                                _pointFound = false;
                            }
                            yield return new WaitForSeconds(DisplaySpeed);
                        }
                }
                    
                if (i != dialogToShow.Dialogs.Length - 1)
                {
                    yield return new WaitForSeconds(TimeBetweenDialogs);
                    DisplayText.text = "";
                }
            }
        }

        else 
        {
            Debug.LogError("Dialog not found"); 
        }
        isDialogRunning = false;
        yield return null; 

    }


}
