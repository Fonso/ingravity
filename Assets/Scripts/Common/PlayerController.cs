﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    /// <summary>
    /// Velocidad a la que se va a mover el jugador
    /// </summary>
    [Tooltip("Velocidad a la que se va a mover el jugador")]
    [Header("Movimiento del jugador")]
    public float MoveSpeed;

    private float _ogMoveSpeed; 
    /// <summary>
    /// Fuerza del salto del jugador. A más valor más salto 
    /// </summary>
    [Tooltip("Fuerza del salto del jugador. A más valor más salto ")]
    [Header("Salto")]
    public float JumpForce;

    public float JumpMoveSpeed; 

    /// <summary>
    /// Define el radio de una esfera invisible a los pies del jugador que nos sirve para saber si esta tocando el suelo o no. 
    /// </summary>
    [Tooltip("Define el radio de una esfera invisible a los pies del jugador que nos sirve para saber si esta tocando el suelo o no.")]
    public float GroundCheckRadius;
    /// <summary>
    /// Define como de cerca o lejos del suelo esta una esfera invisible del suelo a los pies del jugador que nos sirve para saber si esta tocando el suelo o no. 
    /// </summary>
    [Tooltip("Define como de cerca o lejos del suelo esta una esfera invisible del suelo a los pies del jugador que nos sirve para saber si esta tocando el suelo o no.")]
    public float GroundCheckOffset; 

    public LayerMask GroundLayer;

    public bool JumpUnlocked = false; 

    /// <summary>
    /// Referencia al rigidbody del jugador 
    /// </summary>
    private Rigidbody _rb; 
    /// <summary>
    /// Referencia al collider del jugador 
    /// </summary>
    private Collider  _col; 
    /// <summary>
    /// Dirección a la que se debe mover el jugador
    /// </summary>
    private Vector3 _moveDirection = Vector3.zero;
    /// <summary>
    /// Posicion de la esfera que define si el jugador esta lo suficientemente cerca del suelo para saltar o no 
    /// </summary>
    private Vector3 _groundCheckPosition; 
    /// <summary>
    /// Define si el jugador cumple las condiciones para poder saltar
    /// </summary>
    private bool _shouldJump;

    private bool _isJumping; 

    void Awake()
    {
        _ogMoveSpeed = MoveSpeed; 
        _rb  = GetComponentInChildren<Rigidbody>();
        _col = GetComponentInChildren<Collider>();
        _groundCheckPosition = transform.position + new Vector3(0.0f, _col.bounds.extents.y + GroundCheckOffset, 0.0f); 
    }

    void Update()
    { 
        CheckInput();
        LockColliderRotation();
    }

    private void LateUpdate()
    {
        Move();
        Jump();
        CheckIsGrounded(); 
    }

    private void CheckIsGrounded() 
    {
        if (IsGrounded())
        {
            MoveSpeed = _ogMoveSpeed; 
        }

        else 
        {
            MoveSpeed = JumpMoveSpeed; 
        }
    }

    /// <summary>
    /// Corrige la rotación del collider para que no pueda girar en el eje Y 
    /// </summary>
    private void LockColliderRotation() 
    {
        _col.transform.rotation = Quaternion.Euler(_rb.rotation.x, 0.0f, _rb.rotation.z); 
    }

    /// <summary>
    /// Metodo para saber que teclas ha pulsado el jugador y activar el salto o el movimiento si fuera necesario
    /// </summary>
    private void CheckInput() 
    {
        //Check Player Movement
        //Is for pc so best gest axis raw than get axis. 
        float x = Input.GetAxisRaw("Horizontal");
        float z = Input.GetAxisRaw("Vertical"); 

        if(x != 0.0f || z != 0.0f) 
        {
            Vector3 forward = transform.forward.normalized * z;
            Vector3 right   = transform.right.normalized   * x;
            Vector3 result = (forward + right); 

            _moveDirection = result - new Vector3(0.0f, result.y, 0.0f);
        }
        else { _moveDirection = Vector3.zero; }

        //Check Jump
        if(JumpUnlocked && Input.GetButtonDown("Jump") && IsGrounded()) 
        {
            _shouldJump = true; 
        }

        _groundCheckPosition = transform.position + new Vector3(0.0f, _col.bounds.extents.y + GroundCheckOffset, 0.0f);
    }

    public void UnlockJump() 
    {
        JumpUnlocked = true; 
    }

    /// <summary>
    /// Metodo para mover al jugador 
    /// </summary>
    private void Move() 
    {
        Vector3 ground_velocity = (_moveDirection.normalized * MoveSpeed) * Time.fixedDeltaTime;

        _rb.velocity = ground_velocity + new Vector3(0.0f, _rb.velocity.y, 0.0f);
        //_rb.AddForce(ground_velocity + new Vector3(0.0f, _rb.velocity.y, 0.0f), ForceMode.Impulse); 
    }

    /// <summary>
    /// Metodo para hacer saltar al jugador
    /// </summary>
    private void Jump() 
    {
        if (_shouldJump) 
        {
            _rb.AddForce(Vector3.up * JumpForce, ForceMode.Impulse); 
            _shouldJump = false;
            _isJumping = true;
            MoveSpeed = JumpMoveSpeed;
        }
    }

    /// <summary>
    /// Metodo para saber si el jugador esta en el suelo o no 
    /// </summary>
    private bool IsGrounded() 
    {
        _groundCheckPosition = transform.position + new Vector3(0.0f, _col.bounds.extents.y + GroundCheckOffset, 0.0f); 
        if (Physics.OverlapSphere(_groundCheckPosition, GroundCheckRadius, GroundLayer).Length > 0)
        {
            return true;
        }

        else
        {
            return false;
        }
 
    }

    private void OnDrawGizmosSelected() 
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(_groundCheckPosition, GroundCheckRadius);
        Gizmos.DrawRay(transform.position, transform.forward.normalized * 10.0f); 
    }

}
