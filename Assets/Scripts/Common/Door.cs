using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public enum DoorState { Open, Close}
    public Animator DoorHatchAnimator;
    [SerializeField]
    private DoorState InitialState;
    private DoorState _actualState; 

    private void Start()
    {
        SetDoorState(InitialState);    
    }

    public void SetDoorState(DoorState doorState) 
    {
        _actualState = doorState; 
        switch (doorState) 
        {
            case DoorState.Open:
                Open(true); 
                break;
            case DoorState.Close:
                Open(false); 
                break; 
        }
    }

    public void Open(bool isOpen) 
    {
        if (isOpen) 
        {
            DoorHatchAnimator.SetBool("Open", true); 
        }
        else 
        {
            DoorHatchAnimator.SetBool("Close", true);
        }
    }
}
