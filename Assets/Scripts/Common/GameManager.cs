using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class GameManager : MonoBehaviour
{
    private CustomGravity[] _customObjects;
    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (!_instance) 
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        _customObjects = FindObjectsOfType<CustomGravity>();
    }

    private void OnLevelWasLoaded(int level)
    {
        _customObjects = FindObjectsOfType<CustomGravity>();
    }

    private void ResetCGObjects()
    {
        foreach (CustomGravity c in _customObjects) 
        {
            c.ResetObject(); 
        }
    }

    public void ResetAllObjectsGravity() 
    {
        foreach (CustomGravity c in _customObjects)
        {
            c.ResetGravity();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            Application.Quit(); 
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.P)) 
        {
            SceneManager.LoadScene(2); 
        }

      

        if (Input.GetKeyDown(KeyCode.E)) 
        {
            Debug.Log("Reseting objects"); 
            ResetCGObjects(); 
        }
#endif
    }
}
