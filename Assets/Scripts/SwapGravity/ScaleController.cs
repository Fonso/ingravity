using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleController : MonoBehaviour
{
    public float speed;
    public Vector3 InitialScale; 

    // Start is called before the first frame update
    void Start()
    {
        this.transform.localScale = InitialScale; 
        StartCoroutine(ScaleIn()); 
    }

    private IEnumerator ScaleIn()
    {
        while(this.transform.localScale.x < 1.0f) 
        {
            this.transform.localScale += new Vector3(Time.deltaTime, Time.deltaTime, Time.deltaTime) * speed;
            yield return null; 
        }
        yield return null; 
    }
    
}
