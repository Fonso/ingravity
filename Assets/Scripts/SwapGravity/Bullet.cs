using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float _bulletSpeed;
    public void SetBulletSpeed(float bSpeed) { _bulletSpeed = bSpeed; }

    private Vector3 _movementDirection;
    public void CalculateMovementDirection(Vector3 point) { _movementDirection = point - transform.position; }

    private CustomGravity.GravityDirections _gravityDirection;
    public void SetGravityDirection(CustomGravity.GravityDirections g) { _gravityDirection = g; }

    public ParticleSystem ExplosionEffect;
    public Color color;
    public GameObject Renderer; 

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void Move()
    {
        transform.Translate(_movementDirection * _bulletSpeed * Time.deltaTime, Space.World);
    }

    private void OnTriggerEnter(Collider other)
    {
        CustomGravity c = other.gameObject.GetComponent<CustomGravity>();
        Debug.Log("Touched: " + other.name); 
        Explode(); 
        if (c)
        {
            StartCoroutine(OnCollision(other, c));
        }

    }

    private void Explode() 
    {
        Debug.Log("EXPLODE!"); 
        _bulletSpeed = 0; 
        Instantiate(ExplosionEffect.gameObject, this.transform.position, Quaternion.identity);
        GetComponent<Collider>().enabled = false;
        Renderer.SetActive(false);
    }

    private IEnumerator OnCollision(Collider other, CustomGravity c)
    {
        StartCoroutine(ActivateShaderEffect(other.GetComponent<MeshRenderer>().material));
        yield return new WaitForSeconds(0.25f); 
        c.ChangeGravityDirection(_gravityDirection);
        yield return new WaitForSeconds(2);
        Destroy(this.gameObject);
    }


    private IEnumerator ActivateShaderEffect(Material m)
    {
        float fadeInTime = 0.0f;
        float fadeOutTime = 1.0f;
        float result = 0.0f;
        float speed = 5.0f;

        m.SetColor("_Color", color);
        while (fadeOutTime > 0.0f)
        {
            if (fadeInTime < 1.0f)
            {
                fadeInTime += Time.deltaTime * speed;
                result = Mathf.Clamp(fadeInTime, 0.0f, 1.0f);
            }

            else
            {
                fadeOutTime -= Time.deltaTime * speed;
                result = Mathf.Clamp(fadeOutTime, 0.0f, 1.0f);
            }

            m.SetFloat("_MyTime", result);
            yield return null;
        }

    }
}