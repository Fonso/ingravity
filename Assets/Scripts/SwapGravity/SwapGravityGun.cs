using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapGravityGun : MonoBehaviour
{
    private CustomGravity.GravityDirections _gravityDirection = CustomGravity.GravityDirections.right;

    public void UnlockPrimaryAttack() { LockPrimaryAtk = false;  } 
    public float PrimaryAttackCooldown; 
    public bool  LockPrimaryAtk = false;
    
    public void UnlockSecondaryAttack() { LockSecondaryAtk = false;  }
    public float SecondaryAttackCooldown;
    public bool  LockSecondaryAtk = true;

    public void ThirdAbilityCooldown() { LockThirdAbility = false; }
    public bool LockThirdAbility = true;

    public GameObject Bullet;
    public float BulletSpeed; 
    public Transform  ShootPoint; 


    //Direction marker
    public GameObject MarkerPrefab;
    private GameObject _instancedMarker;
    public bool _directionMarkerVisible = false;

    //public GameObject SecondaryMarkerPrefab;
    //private GameObject _instancedSecondaryMarker;
    //private bool _secondDirectionMarkerVisible = false;

    public ParticleSystem PrimaryGunEffect;  
    public ParticleSystem SecondaryGunEffect;
    public ParticleSystem ThirdAbilityEffect; 

    public GameObject CapturedGArrow;
    public GameObject SelectedGArrow; 

    private bool CapturedArrowRotating;
    private bool SelectedArrowRotating; 

    public Color OGColor; 

    private RaycastHit _lastHit;
    private RaycastHit _markerHit;

    private void OnLevelWasLoaded(int level)
    {
        LockPrimaryAtk = false;
        LockSecondaryAtk = true;
        LockThirdAbility = true;
    }

    private void Start()
    {
        CreateMarkers();
    }

    protected void Update()
    {
        ShowDirectionMarker();
        PrimaryShoot(); 
        SecondaryShoot();
        ThirdAbility(); 
    }

    private void ShowDirectionMarker()
    {
        if (Physics.Raycast(transform.position, transform.forward, out _markerHit, Mathf.Infinity))
        {
            CustomGravity gravity_component = _markerHit.collider.gameObject.GetComponent<CustomGravity>();

            if (gravity_component)
            {
                if (!_directionMarkerVisible && _gravityDirection != CustomGravity.GravityDirections.zero)
                {
                    _directionMarkerVisible = true;
                    if (gravity_component.GravityDirection == _gravityDirection) 
                    {
                        Vector3 new_position = _markerHit.transform.position - (CustomGravity.ToAbsoluteVec3(_gravityDirection).normalized * 5); 
                        RepositionMarker(_gravityDirection, 
                                         new_position, _instancedMarker);
                    }
                    else { RepositionMarker(_gravityDirection, _markerHit.transform.position, _instancedMarker); }



                }

                ChangeArrowRotationSelected(gravity_component.GravityDirection, SelectedGArrow, _markerHit.transform.position);
                ChangeArrowRotationCaptured(_gravityDirection, CapturedGArrow, _markerHit.transform.position);

                //ChangeArrowRotation(_gravityDirection, CapturedGArrow, _markerHit.transform.position);

                //Secondary Shoot
                //if(!_secondDirectionMarkerVisible && _gravityDirection != CustomGravity.GravityDirections.zero) 
                //{
                //    _secondDirectionMarkerVisible = true;
                //    if (gravity_component.GravityDirection == _gravityDirection)
                //    {
                //        Vector3 new_position2 = _markerHit.collider.gameObject.transform.position - (CustomGravity.ToAbsoluteVec3(gravity_component.GravityDirection).normalized);
                //        RepositionMarker(gravity_component.GravityDirection,
                //                         new_position2, _instancedSecondaryMarker);
                //    }
                //    else { RepositionMarker(gravity_component.GravityDirection, _markerHit.transform.position, _instancedSecondaryMarker); }
                //}
            }

            else
            {
                _directionMarkerVisible = false;
                _instancedMarker.SetActive(false);

                //_secondDirectionMarkerVisible = false;
                //_instancedSecondaryMarker.SetActive(false);
            }
        }
    }


    private GameObject CreateMarkers()
    {
        _instancedMarker = Instantiate(MarkerPrefab.gameObject);
        _instancedMarker.SetActive(false);

        //_instancedSecondaryMarker = Instantiate(SecondaryMarkerPrefab.gameObject);
        //_instancedSecondaryMarker.SetActive(false); 

        return _instancedMarker;
    }

    private void ChangeArrowRotationSelected(CustomGravity.GravityDirections direction, GameObject arrow, Vector3 objPos)
    {
        Vector3 new_direction = CustomGravity.ToAbsoluteVec3(direction); 
        if (new_direction != Vector3.zero && !SelectedArrowRotating)
        {
            //StartCoroutine(LerpLocalRotation(arrow, arrow.transform.localRotation, new_rot, result => SelectedArrowRotating = result));
            Quaternion new_rot = Quaternion.LookRotation(new_direction, CustomGravity.ToAbsoluteVec3(CustomGravity.GravityDirections.up));
            Debug.Log(Vector3.Angle(new_direction.normalized, new_rot.eulerAngles.normalized)); 
            if (Vector3.Angle(new_rot.eulerAngles.normalized, new_direction.normalized) > 10.0f)
            {
                StartCoroutine(LerpRotation(arrow, arrow.transform.rotation, new_rot, result => SelectedArrowRotating = result));
            }

            else { arrow.transform.rotation = new_rot;  }

            //StartCoroutine(LerpRotation(arrow, arrow.transform.localRotation, new_rot, result => SelectedArrowRotating = result)); 
        };
    }

    private void ChangeArrowRotationCaptured(CustomGravity.GravityDirections direction, GameObject arrow, Vector3 objPos)
    {
        Vector3 new_direction = CustomGravity.ToAbsoluteVec3(direction);
        if (new_direction != Vector3.zero && !CapturedArrowRotating)
        {
            //arrow.transform.localRotation = Quaternion.LookRotation(new_direction, CustomGravity.ToAbsoluteVec3(CustomGravity.GravityDirections.up));
            Quaternion new_rot = Quaternion.LookRotation(new_direction, CustomGravity.ToAbsoluteVec3(CustomGravity.GravityDirections.up));

            if (Vector3.Angle(new_rot.eulerAngles.normalized, new_direction.normalized) > 10.0f)
            {
                StartCoroutine(LerpRotation(arrow, arrow.transform.rotation, new_rot, result => CapturedArrowRotating = result));
            }
            else 
            {
                arrow.transform.rotation = new_rot;
            }
        }; 
    }


    private void ChangeArrowRotation(CustomGravity.GravityDirections direction, GameObject arrow, Vector3 objPos) 
    {
        Vector3 new_direction = CustomGravity.ToRelativeVector3(direction, transform.position, objPos);
        bool temp = false; 
        if (new_direction != Vector3.zero)
        {
            //arrow.transform.localRotation = Quaternion.LookRotation(new_direction, CustomGravity.ToAbsoluteVec3(CustomGravity.GravityDirections.up));
            Quaternion new_rot = Quaternion.LookRotation(new_direction, CustomGravity.ToAbsoluteVec3(CustomGravity.GravityDirections.up));
            StartCoroutine(LerpLocalRotation(arrow, arrow.transform.localRotation, new_rot, result => temp = result));
        }
    }

    private IEnumerator LerpLocalRotation(GameObject obj, Quaternion firstRot, Quaternion secondRot, System.Action<bool>isRotating) 
    {
        float t = 0;
        isRotating(true);

        while (t < 1.0f) 
        {

            t += Time.deltaTime; 
            obj.transform.localRotation = Quaternion.Lerp(firstRot, secondRot, t);
            yield return null; 
        }
        isRotating(false);
    }

    private IEnumerator LerpRotation(GameObject obj, Quaternion firstRot, Quaternion secondRot, System.Action<bool> isRotating)
    {
        float t = 0;
        isRotating(true);

        while (t < 1.0f)
        {
            t += Time.deltaTime;
            obj.transform.rotation = Quaternion.Lerp(firstRot, secondRot, t); 
            yield return null;
        }
        isRotating(false);
    }


    private void ChangeArrowRotation(CustomGravity.GravityDirections direction, GameObject arrow) 
    {
        //Vector3 new_direction = CustomGravity.ToRelativeVector3(direction, transform.position, objPos);
        //if (new_direction != Vector3.zero)
        //{
        //    arrow.transform.localRotation = Quaternion.LookRotation(new_direction, CustomGravity.ToAbsoluteVec3(CustomGravity.GravityDirections.up));
        //}
    }

    private void RepositionMarker(CustomGravity.GravityDirections direction, Vector3 position, GameObject marker)
    {
        marker.SetActive(true);

        marker.transform.rotation = Quaternion.LookRotation(CustomGravity.GravityDirectionToVector3(direction));

        marker.transform.position = position;
    }

    private void PrimaryShoot()
    {
        if (!LockPrimaryAtk && Input.GetMouseButtonDown(0))
        {
            StartCoroutine(Shoot());    
        }
    }

    private IEnumerator Shoot() 
    {
        PrimaryGunEffect.Play();

        yield return new WaitForSeconds(0.15f);  
        GameObject Go = GameObject.Instantiate(Bullet, ShootPoint.transform.position, Quaternion.identity);
        Bullet b = Go.GetComponent<Bullet>();

        b.SetBulletSpeed(BulletSpeed);

        RaycastHit hit;
        Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity);
        b.CalculateMovementDirection(hit.point);
        b.SetGravityDirection(this._gravityDirection);

        Go.transform.forward = this.transform.forward;
        StartCoroutine(ActivateCooldown(result => LockPrimaryAtk = result, PrimaryAttackCooldown));
        yield return null; 
    }


    private void SecondaryShoot()
    {
        if (!LockSecondaryAtk && Input.GetMouseButton(1))
        {
            if (Physics.Raycast(transform.position, transform.forward, out _lastHit, Mathf.Infinity))
            {
                CustomGravity c = _lastHit.transform.gameObject.GetComponent<CustomGravity>();
                if (c)
                {
                    this._gravityDirection = c.GravityDirection;
                    if (c.GravityDirection == _gravityDirection)
                    {
                        Vector3 new_position = _lastHit.transform.position - (CustomGravity.ToAbsoluteVec3(c.GravityDirection).normalized * 5);
                        RepositionMarker(_gravityDirection,
                                         new_position, _instancedMarker);

                        //Vector3 new_position2 = _lastHit.transform.position - (CustomGravity.ToAbsoluteVec3(c.GravityDirection).normalized);
                        //RepositionMarker(_gravityDirection,
                        //                 new_position2, _instancedSecondaryMarker);
                    }
                    else 
                    {   
                        RepositionMarker(_gravityDirection, _lastHit.transform.position, _instancedMarker);
                        //RepositionMarker(_gravityDirection, _lastHit.transform.position, _instancedSecondaryMarker);
                    }

                    SecondaryGunEffect.Stop(); 
                    SecondaryGunEffect.Play(); 
                    StartCoroutine(ActivateShaderEffect(c.GetComponent<Renderer>().material));
                    StartCoroutine(ActivateCooldown(result => LockSecondaryAtk = result, SecondaryAttackCooldown)); 
                }
            }
        }
    }

    private IEnumerator ActivateCooldown(System.Action<bool> cdBoolean, float time) 
    {
        cdBoolean(true);
        yield return new WaitForSeconds(time);
        cdBoolean(false); 
    }


    private IEnumerator ActivateShaderEffect(Material m) 
    {
        float fadeInTime = 0.0f;
        float fadeOutTime = 1.0f;
        float result = 0.0f;
        float speed = 1.0f;

        m.SetColor("_Color", OGColor);
        while (fadeOutTime > 0.0f) 
        {
            if (fadeInTime < 1.0f)
            {
                fadeInTime += Time.deltaTime * speed;
                result = Mathf.Clamp(fadeInTime, 0.0f, 1.0f);
            }

            else 
            {
                fadeOutTime -= Time.deltaTime * speed;
                result = Mathf.Clamp(fadeOutTime, 0.0f, 1.0f);
            }

            m.SetFloat("_MyTime", result);
            yield return null; 
        }
    }

    private void ThirdAbility()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            GameManager.Instance.ResetAllObjectsGravity();
            ThirdAbilityEffect.Play(); 
        }
    }

}
