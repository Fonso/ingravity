using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunControllerV3 : Gun
{
    private CustomGravity.GravityDirections _gravityDirection = CustomGravity.GravityDirections.right;
    private HUDController _hudController;
    private bool _directionMarkerVisible = false;
    private Transform _markerSelected;
    public bool LockSecondaryAt = false; 
    public ParticleSystem SecondaryShootParticles;
    public GameObject MarkerPrefab;
    public GameObject Bullet; 


    private GameObject _instancedMarker; 



    private void Start()
    {
        if (!_hudController) { _hudController = GetComponentInChildren<HUDController>(); }
        _hudController.UpdateImage(_gravityDirection);
        CreateMarker(); 
    }

    protected override void Update()
    {
        base.Update();
        ShowDirectionMarker(); 
        SecondaryShoot();
        UpdateMarkerPosition(); 
    }

    private void ShowDirectionMarker() 
    {
        RaycastHit hit;
        LayerMask l = 1 << 8; 
        if (Physics.Raycast(transform.position, transform.forward, out hit, Mathf.Infinity, ~(LayerMask.GetMask("NoCollisionBullet"))))
        {
            CustomGravity gravity_component = hit.collider.gameObject.GetComponent<CustomGravity>();

            if (gravity_component)
            {
                if (!_directionMarkerVisible && _gravityDirection != CustomGravity.GravityDirections.zero)
                {
                    _directionMarkerVisible = true;
                    _markerSelected = hit.transform; 
                    RepositionMarker(_gravityDirection, hit.transform.position); 
                }
            }

            else
            {
                _directionMarkerVisible = false;
                _instancedMarker.SetActive(false); 
            }
        }

  
    }

    private void UpdateMarkerPosition()
    {
        if (_markerSelected)
        {
            _instancedMarker.transform.position = _markerSelected.position;
        }
    }

    private GameObject CreateMarker()
    {
        _instancedMarker = Instantiate(MarkerPrefab.gameObject);
        _instancedMarker.SetActive(false); 

        return _instancedMarker;
    }

    private void RepositionMarker(CustomGravity.GravityDirections direction, Vector3 position) 
    {
        _instancedMarker.SetActive(true);

        _instancedMarker.transform.rotation = Quaternion.LookRotation(CustomGravity.GravityDirectionToVector3(direction));

        _instancedMarker.transform.position = position;
    }


    private void SecondaryShoot() 
    {
        if (!LockSecondaryAt && Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(transform.position, transform.forward, out _lastHit, Mathf.Infinity))
            {
                Instantiate(SecondaryShootParticles, _lastHit.point, Quaternion.identity);
                CustomGravity gravity_component = _lastHit.collider.gameObject.GetComponent<CustomGravity>();

                if (gravity_component)
                {
                    gravity_component.ChangeGravityDirection(_gravityDirection); 
                }
          
            }
        }
    }

    protected override void OnPostHit()
    {
        base.OnPostHit();
        if (_selected) 
        { _gravityDirection = _selected.GravityDirection; }
        _hudController.UpdateImage(_gravityDirection);
        StartCoroutine(ActivateShaderEffect(1.0f, _lastHit.transform.gameObject.GetComponent<Renderer>())); 
    }

    private IEnumerator ActivateShaderEffect(float duration, Renderer rend) 
    {
        float timeElapsed = 0f;
        float speed = Mathf.Abs(rend.material.GetFloat("_Speed"));
        float phase = rend.material.GetFloat("_MyTime");
        float targetPhase = 1 / speed;

        while (timeElapsed <= duration)
        {
            timeElapsed += Time.deltaTime;
            rend.material.SetFloat("_Phase", Mathf.Lerp(phase, targetPhase, timeElapsed / duration));
            yield return new WaitForEndOfFrame();
        };
    }

    public void UnlockSecondaryAttack() 
    {
        LockSecondaryAt = false;     
    }

}
