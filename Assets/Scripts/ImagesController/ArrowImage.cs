using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI; 

public class ArrowImage : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private static ArrowImage currentSelected = null;
    public CustomGravity.GravityDirections Direction;

    Image _mat; 

    private void Start()
    {
        currentSelected = this; 
        _mat = GetComponent<Image>(); 
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        changeMaterialAlpha(0.5f); 
        currentSelected = this;
    }

    public void ResetMaterialAlpha()
    {
        changeMaterialAlpha(1.0f);  
    }

    private void changeMaterialAlpha(float new_alpha)
    {
        Color color = _mat.color;
        color.a = new_alpha;
        _mat.color = color;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        changeMaterialAlpha(1.0f);
    }

    public static ArrowImage GetCurrentSelected() 
    {
        return currentSelected; 
    }

}
