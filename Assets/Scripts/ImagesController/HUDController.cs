using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class HUDController : MonoBehaviour
{
    public Image ActualDirection; 
    public GameObject DirectionSelector;
    public CameraController CameraController;
    public Sprite RightImage;
    public Sprite FrontImage;

    private ArrowImage []_arrowImages;
    private DialogController _dialogController; 

    private void Awake()
    {
        FindArrowImages();
    }

    private void FindArrowImages() 
    {
        _arrowImages = new ArrowImage[6];
        _arrowImages = DirectionSelector.gameObject.GetComponentsInChildren<ArrowImage>();  
    }

    public void ShowArrows(bool show) 
    {
        if (!show) 
        {
            foreach (ArrowImage r in _arrowImages) 
            {
                r.ResetMaterialAlpha(); 
            }
        }
        if(ArrowImage.GetCurrentSelected() != null) 
        {
            UpdateImage(ArrowImage.GetCurrentSelected().Direction);
        }
        DirectionSelector.SetActive(show);
        CameraController.LockCamera(show);
        if (show) { CameraController.ChangeCursorLockMode(CursorLockMode.None);   Cursor.visible = true; }
        else {      CameraController.ChangeCursorLockMode(CursorLockMode.Locked); Cursor.visible = false;  }
    }

    public void UpdateImage(CustomGravity.GravityDirections direction) 
    {
        switch (direction) 
        {
            case CustomGravity.GravityDirections.up:
                ActualDirection.sprite = RightImage;
                ActualDirection.rectTransform.rotation = Quaternion.Euler(new Vector3(0,0,90)); 
                break;
            case CustomGravity.GravityDirections.down:
                ActualDirection.sprite = RightImage;
                ActualDirection.rectTransform.rotation = Quaternion.Euler(new Vector3(0, 0, -90)); 
                break;
            case CustomGravity.GravityDirections.right:
                ActualDirection.sprite = RightImage;
                ActualDirection.rectTransform.rotation = Quaternion.Euler(new Vector3(0, 0, 0)); 
                break;
            case CustomGravity.GravityDirections.left:
                ActualDirection.sprite = RightImage;
                ActualDirection.rectTransform.rotation = Quaternion.Euler(new Vector3(0, 0, 180));
                break;
            case CustomGravity.GravityDirections.forward:
                ActualDirection.sprite = FrontImage;
                ActualDirection.rectTransform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                break;
            case CustomGravity.GravityDirections.back:
                ActualDirection.sprite = FrontImage;
                ActualDirection.rectTransform.rotation = Quaternion.Euler(new Vector3(0, 0, 180));
                break; 
        }
    }
}
