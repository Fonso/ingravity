using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunControllerV2 : Gun
{
    private HUDController _hudController;
    private CustomGravity.GravityDirections _gravityDirection = CustomGravity.GravityDirections.down;

    private void Start()
    {
        if(!_hudController) _hudController = GetComponentInChildren<HUDController>();
    }

    protected override void Update()
    {
        base.Update();
        ShowHUD(); 
    }

    private void ShowHUD() 
    {

        if (Input.GetMouseButtonDown(1))
        {
            _hudController.ShowArrows(true);
        }
        else if (Input.GetMouseButtonUp(1))
        {
            _hudController.ShowArrows(false);
            _gravityDirection = ArrowImage.GetCurrentSelected().Direction;
        }
    }

    protected override void OnPostHit()
    {
        base.OnPostHit();
        if (_selected)
        {
            _selected.ChangeGravityDirection(_gravityDirection, this.gameObject);
        }
    }

}
